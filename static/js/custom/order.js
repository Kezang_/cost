

// $(document).ready(function () {
//     $.ajax({
//         url: "http://localhost:5000/get_equipment", // Update URL with your Flask server URL
//         type: "GET",
//         success: function(response) {
//             // Populate table with equipment data
//             if (response) {
//                 var table = '';
//                 $.each(response, function(index, equipment) {
//                     table += '<tr>' +
//                         '<td>' + equipment.Equipmen_names + '</td>' +
//                         '<td>' + equipment.Remarks + '</td>' +
//                         '<td>' + equipment.price + '</td>' +
//                         '<td>' + equipment.cost_Data_reference + '</td>' +
//                         '</tr>';
//                 });
//                 $("#equipmentTable tbody").html(table);
//             }
//         },
//         error: function(xhr, status, error) {
//             console.error(xhr.responseText);
//             // Handle error response
//         }
//     });

//     // // Load equipment data when the document is ready
//     // loadEquipmentData();

//     // Save Equipment
//     $("#saveEquipment").on("click", function () {
//         var equipmentData = {
//             name: $("#name").val(),
//             price: $("#price").val(),
//             remarks: $("#remarks").val(),
//             cost_reference: $("#cost_reference").val()
//         };

//         $.ajax({
//             type: "POST",
//             url: "/insert_equipment",
//             data: JSON.stringify(equipmentData),
//             contentType: "application/json",
//             success: function (response) {
//                 // Reload equipment data after successful insertion
//                 loadEquipmentData();
//                 $("#productModal").modal("hide");
//             },
//             error: function (xhr, status, error) {
//                 console.error(xhr.responseText);
//                 // Handle error response
//             }
//         });
//     });

//     // Delete Equipment
//     $(document).on("click", ".delete-equipment", function () {
//         var equipmenName = $(this).data("id");
//         var isDelete = confirm("Are you sure to delete this equipment?");
//         if (isDelete) {
//             $.ajax({
//                 type: "POST",
//                 url: "/delete_equipment",
//                 data: JSON.stringify({ Equipmen_names: equipmenName }),
//                 contentType: "application/json",
//                 success: function (response) {
//                     // Reload equipment data after successful deletion
//                     loadEquipmentData();
//                 },
//                 error: function (xhr, status, error) {s
//                     console.error(xhr.responseText);
//                     // Handle error response
//                 }
//             });
//         }
//     });
// });
