from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def home():
    # Render the simplecalculator.html template
    return render_template('simplecalculator.html')

if __name__ == '__main__':
    app.run(debug=True)
